package kadai
package result

import Invalid.syntax._
import scala.util.control.NonFatal
import scalaz.{ Applicative, Catchable, EitherT, Functor, \/ }
import scalaz.Scalaz.Id
import scalaz.syntax.either._
import scalaz.syntax.std.boolean._

trait ResultTypes {
  /**
   * Base result (monad transformer) type for services. Specializes EitherT to be either an Error (not found, invalid
   * request or some unknown error) or the value itself.
   */
  type ResultT[F[_], A] = EitherT[F, Invalid, A]

  object ResultT {
    import scalaz.syntax.applicative._

    def apply[F[_], A](a: F[Invalid \/ A]): ResultT[F, A] =
      EitherT.eitherT(a)

    def left[F[_], A](a: F[Invalid])(implicit F: Functor[F]): ResultT[F, A] =
      EitherT.left(a)

    def right[F[_], A](b: F[A])(implicit F: Functor[F]): ResultT[F, A] =
      EitherT.right(b)

    /** Evaluate the given value, which might throw an exception. */
    def catching[F[_]: Applicative, A](a: => A): ResultT[F, A] =
      try right { a.point[F] } // need to do this explicitly due to thunk expansion, otherwise weird error in scalac about implicit conversions
      catch { case NonFatal(ex) => left { ex.invalid.point[F] } }

    implicit def CatachableResultT[F[_]: Applicative]: Catchable[ResultT[F, ?]] =
      new Catchable[ResultT[F, ?]] {
        def attempt[A](f: ResultT[F, A]): ResultT[F, Throwable \/ A] =
          right {
            f.fold(_.toThrowable.left[A], _.right[Throwable])
          }

        def fail[A](err: Throwable): ResultT[F, A] =
          left(err.invalid.point[F])
      }
  }

  def catching[A](a: => A): Result[A] =
    Result.catchingToResult(a)

  type Result[A] = ResultT[Id, A]

  object Result {
    import scalaz.syntax.applicative._

    def right[A](a: => A): Result[A] =
      ResultT.right(a.point[Id])

    def left[A](i: Invalid): Result[A] =
      ResultT.left(i.point[Id])

    def apply[A](a: Invalid \/ A): Result[A] =
      ResultT(a.point[Id])

    /** Evaluate the given value, which might throw an exception. */
    def catchingToResult[A](a: => A): Result[A] =
      ResultT.catching(a) // need to do this explicitly due to thunk expansion, otherwise weird error in scalac about implicit conversions
  }

  object syntax {

    implicit class AddResultSyntax[A](val a: A) {
      def result: Result[A] = Result(\/.right(a))
    }

    implicit class ResultOptionSyntax[A](opt: Option[A]) {
      def resultOr(f: => Invalid): Result[A] = opt.fold { f.failureResult[A] } { _.result }
    }

    implicit class ResultBooleanSyntax(b: Boolean) {
      def foldResult[A](t: => A, f: => Invalid): Result[A] = b.fold(t.result, f.failureResult[A])
    }

    /**
     * Helper trait and classes to create a failure from exceptions or messages.
     */
    sealed trait ConvertTo {
      protected def failure: Invalid
      final def failureResult[A]: Result[A] = ResultT.left[Id, A](failure)
    }

    implicit class AddFailureThrowable(e: Throwable) extends ConvertTo {
      protected override def failure = e.invalid
    }

    implicit class AddFailureString(m: String) extends ConvertTo {
      protected override def failure = m.invalid
    }

    implicit class AddFailureFailure(m: Invalid) extends ConvertTo {
      protected override def failure = m
    }
  }
}