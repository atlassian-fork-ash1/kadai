import sbt._
import Keys._

object Dependencies {

  object Version {
    val scala212       = "2.12.2"
    val scala211       = "2.11.8"
    val scalaz         = "7.2.12"
    val specs2         = "3.8.7"
    val scalacheck     = "1.13.5"
    val junit          = "4.12"
    val scodecBits     = "1.1.4"
    val typesafeConfig = "1.3.1"
    val shapeless      = "2.3.2"
    val nscalaTime     = "2.16.0"
  }

  lazy val commonTest = 
    Seq(
      "org.specs2"          %% "specs2-core"        % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-junit"       % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-scalacheck"  % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-mock"        % Version.specs2     % "test"
    , "org.scalacheck"      %% "scalacheck"         % Version.scalacheck % "test"
    , "junit"                % "junit"              % Version.junit      % "test"
    )

  lazy val scalaz =
    Seq(
      "org.scalaz"        %% "scalaz-core"               % Version.scalaz
    , "org.scalaz"        %% "scalaz-concurrent"         % Version.scalaz
    , "org.scalaz"        %% "scalaz-effect"             % Version.scalaz
    )

  lazy val typesafeConfig =
    Seq("com.typesafe"              % "config"      % Version.typesafeConfig)

  lazy val scodecBits = 
    Seq("org.scodec"        %% "scodec-bits"        % Version.scodecBits)

  lazy val shapeless = 
    Seq("com.chuusai"   %% "shapeless"   % Version.shapeless)

  lazy val nscalaTime = 
    Seq("com.github.nscala-time"   %% "nscala-time" % Version.nscalaTime)
}
