package kadai

import Invalid.syntax._
import org.scalacheck.rng.Seed
import org.scalacheck.{Arbitrary, Cogen, Gen, Prop}

import scalaz.\/

object Arbitraries {
  import Invalid._

  implicit def ArbitraryInvalid: Arbitrary[Invalid] =
    Arbitrary {
      Gen.oneOf(
        Arbitrary.arbitrary[Throwable].map { _.invalid },
        Arbitrary.arbitrary[String].map { _.invalid }
      )
    }

  implicit def arbitraryAttempt[A: Arbitrary]: Arbitrary[Attempt[A]] =
    Arbitrary {
      import Attempt._

      Gen.frequency(
        1 -> Arbitrary.arbitrary[Throwable].map { exception },
        1 -> Arbitrary.arbitrary[String].map { fail },
        9 -> Arbitrary.arbitrary[A].map { ok }
      )
    }

  implicit def cogenDisjunction[A, B](implicit A: Cogen[A], B: Cogen[B]): Cogen[A \/ B] =
    Cogen((seed: Seed, e: A \/ B) => e.fold(a => A.perturb(seed, a), b => B.perturb(seed.next, b)))

  implicit def cogenInvalid: Cogen[Invalid] =
    Cogen(i => i match {
      case Message(_) => 0L
      case Err(_) => 1L
      case Composite(_) => 2L
      case Zero => 3L
    })
}
