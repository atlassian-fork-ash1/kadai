/* 
 * Copyright 2012 Atlassian PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kadai
package config

import java.io.{ File, Serializable }
import java.util.concurrent.TimeUnit

import result.{ Result, ResultT }
import scala.annotation.implicitNotFound
import collection.JavaConverters.{ asScalaBufferConverter, asScalaSetConverter }
import concurrent.duration._
import reflect.ClassTag
import util.control.Exception
import com.typesafe.config.{ Config, ConfigFactory, ConfigObject, ConfigParseOptions, ConfigResolveOptions, ConfigException }
import scalaz.{ Applicative, Functor, OptionT }
import scalaz.Scalaz.Id
import scalaz.syntax.id._

/**
 * Simple user-friendly wrapper around Config.
 *
 * Provides a single type-safe apply method for getting values out from the configuration.
 *
 * Usage:
 * {{{
 * val config = Configuration.load("filename.conf").get[Configuration]("objectName")
 * val intThing = config[Int]("intPropertyName")
 * val strThing = config[String]("stringPropertyName")
 * }}}
 * Note that formatting or other problems will throw exceptions.
 *
 * You can also optionally find correct config items or validate and check their correctness (with Either):
 * {{{
 * val intOption:Option[Int] = config.option[Int]("intPropertyName")
 * val strThing: Either[Throwable, String] = config.valid[String]("stringPropertyName")
 * }}}
 *
 * The Accessor type-classes implement the glue to get the specific type configuration item.
 *
 * Details on the underlying configuration file specification can be found here:
 * https://github.com/typesafehub/config/blob/master/HOCON.md
 */
trait ConfigurationInstances {

  val failIfMissing =
    ConfigParseOptions.defaults.setAllowMissing(false)

  def apply(c: Config) =
    new Configuration(c)

  def from(s: String) =
    Configuration(ConfigFactory.parseString(s).resolve)

  def from(f: File) =
    Configuration {
      ConfigFactory.defaultOverrides.withFallback(ConfigFactory.parseFile(f, failIfMissing).resolve)
    }

  /** The path is always relative and on the classpath. */
  def load(path: String) =
    Configuration {
      ConfigFactory.defaultOverrides.withFallback {
        ConfigFactory.load(path, failIfMissing, ConfigResolveOptions.defaults) // by default resolves
      }
    }

  /** The type-class that is used to extract a config item of a particular type. */
  @implicitNotFound(msg = "Cannot find Configuration.Accessor for type ${A} – it is needed to extract config text and instantiate an element of that type")
  trait Accessor[A] {
    self =>
    def apply(c: Config, s: String): A

    final def map[B](f: A => B): Accessor[B] =
      new Accessor[B] {
        def apply(c: Config, s: String): B =
          f(self(c, s))
      }
  }

  object Accessor {
    def apply[A: Accessor]: Accessor[A] =
      implicitly[Accessor[A]]

    implicit object AccessorFunctor extends Functor[Accessor] {
      def map[A, B](fa: Accessor[A])(f: A => B): Accessor[B] = fa map f
    }
  }

  //
  // standard type-class instances
  //

  implicit object IntAccessor extends Accessor[Int] {
    def apply(c: Config, s: String) = c getInt s
  }
  implicit object DoubleAccessor extends Accessor[Double] {
    def apply(c: Config, s: String) = c getDouble s
  }
  implicit object StringAccessor extends Accessor[String] {
    def apply(c: Config, s: String) = c getString s
  }
  implicit object SeqStringAccessor extends Accessor[Seq[String]] {
    def apply(c: Config, s: String) = c getString s split ","
  }
  implicit object ListStringAccessor extends Accessor[List[String]] {
    def apply(c: Config, s: String) = c.getStringList(s).asScala.toList
  }
  implicit object LongAccessor extends Accessor[Long] {
    def apply(c: Config, s: String) = c getLong s
  }
  implicit object BooleanAccessor extends Accessor[Boolean] {
    def apply(c: Config, s: String) = c getBoolean s
  }

  import org.joda.time.DateTime
  implicit object DateTimeAccessor extends Accessor[DateTime] {
    def apply(c: Config, s: String) = new DateTime(c.getDuration(s, TimeUnit.MILLISECONDS))
  }
  implicit object FileAccessor extends Accessor[File] {
    def apply(c: Config, s: String) = new File(c getString s)
  }
  implicit object ConfigAccessor extends Accessor[Config] {
    def apply(c: Config, s: String) = c getConfig s
  }
  implicit object ConfigurationAccessor extends Accessor[Configuration] {
    def apply(c: Config, s: String) = Configuration(c getConfig s)
  }
  implicit object ConfigObjectAccessor extends Accessor[ConfigObject] {
    def apply(c: Config, s: String) = c getObject s
  }
  implicit def ClassAccessor[A: ClassTag] = new Accessor[Class[A]] {
    def apply(c: Config, s: String): Class[A] = {
      val cls = Class.forName(c getString s).asInstanceOf[Class[A]]
      implicitly[ClassTag[A]].runtimeClass |> { expect =>
        if (!(expect isAssignableFrom cls))
          throw new ClassCastException("%s must be a subclass of %s (found [%s])".format(s, expect, cls))
      }
      cls
    }

  }
  implicit def ResultTAccessor[F[_]: Applicative, A: Accessor] = new Accessor[ResultT[F, A]] {
    def apply(c: Config, s: String): ResultT[F, A] =
      ResultT.catching { Accessor[A].apply(c, s) }
  }
  implicit def OptionTAccessor[F[_]: Applicative, A: Accessor] = new Accessor[OptionT[F, A]] {
    def apply(c: Config, s: String): OptionT[F, A] =
      Accessor[ResultT[F, A]].apply(c, s).toOption
  }
  implicit def OptionAccessor[A: Accessor] = new Accessor[Option[A]] {
    def apply(c: Config, s: String): Option[A] =
      Accessor[OptionT[Id, A]].apply(c, s).run
  }
  implicit def ConfigReaderAccessor[A: ConfigReader]: Accessor[A] = new Accessor[A] {
    def apply(c: Config, s: String) =
      Configuration(c)[Configuration](s) |> { c => ConfigReader.run(c) }
  }

  /**
   * Accessor for TimeUnits. This matches what typesafe config expects (see com.typesafe.config.impl.SimpleConfig),
   * and adds some full singular form and medium abbreviation matchers.
   */
  implicit object TimeUnitAccessor extends Accessor[TimeUnit] {
    def apply(c: Config, s: String) =
      c.getString(s) match {
        case "ms" | "milli" | "millis" | "millisecond" | "milliseconds" => TimeUnit.MILLISECONDS
        case "us" | "micro" | "micros" | "microsecond" | "microseconds" => TimeUnit.MICROSECONDS
        case "ns" | "nano" | "nanos" | "nanosecond" | "nanoseconds" => TimeUnit.NANOSECONDS
        case "d" | "day" | "days" => TimeUnit.DAYS
        case "h" | "hr" | "hour" | "hours" => TimeUnit.HOURS
        case "s" | "sec" | "secs" | "second" | "seconds" => TimeUnit.SECONDS
        case "m" | "min" | "mins" | "minute" | "minutes" => TimeUnit.MINUTES
        case unknown => throw new ConfigException.BadValue(s, s"Could not parse time unit $unknown. Try ns, us, ms, s, m, d")
      }
  }

  implicit object DurationAccessor extends Accessor[Duration] {
    def apply(c: Config, s: String): Duration = Duration.fromNanos(c.getDuration(s, TimeUnit.NANOSECONDS))
  }

  def asString(c: Configuration): String = c.toConfig.root.render

  private[config] val catcher = Exception.nonFatalCatch
}

class Configuration protected[config] (val c: Config) extends Serializable {
  import Configuration._

  def apply[A: Accessor](s: String): A =
    implicitly[Accessor[A]].apply(c, s)

  def get[A: Accessor](s: String): A =
    implicitly[Accessor[A]].apply(c, s)

  def option[A: Accessor](s: String): Option[A] =
    catcher.opt {
      implicitly[Accessor[A]].apply(c, s)
    }

  def valid[A: Accessor](s: String): Either[Throwable, A] =
    catcher.either {
      implicitly[Accessor[A]].apply(c, s)
    }

  def keys(s: String): Iterable[String] =
    implicitly[Accessor[ConfigObject]].apply(c, s).keySet.asScala

  private[kadai] def config(s: String): Config =
    apply[Config](s)

  def toConfig: Config = c

  def overriding(as: (String, String)*) =
    Configuration {
      import collection.JavaConverters._
      ConfigFactory.parseMap(as.toMap.asJava).withFallback(c)
    }

  def withFallback(other: Configuration) = Configuration(c withFallback other.c)

  override def toString = c.root.toString
  override def equals(a: Any) = a.isInstanceOf[Configuration] && c == a.asInstanceOf[Configuration].c
  override def hashCode = c.hashCode

  private def writeReplace: Object = new SerializationProxy(asString(this))
}

object Configuration extends ConfigurationInstances {
  private[kadai] class SerializationProxy(s: String) extends Serializable {
    def readResolve: Object = from(s)
  }
}
