# config

A library for dealing with configuration files.

This library wraps the typesafe-config lib and the underlying format is its
HOCON format. Please see that library for details and documentation of
the underlying format.

## Configuration

`Configuration` is a wrapper around a typesafe-config `Config` object that allows
extraction of configuration options in a far more convenient manner.

Configuration options can be extracted by type simply by supplying the type
parameter if an instance of the typeclass `Configuration.Accessor[A]` exists 
for the type parameter `A`. For instance, given the config file:

    val config = Configuration.from { """
      some {
        name = Alice
        age = 32
      }
      """
    }

the following work:

    val name = config[String]("some.name")  // Alice
    val age  = config[Int]("some.age")      // 32
    val flub = config[Int]("some.flub")     // com.typesafe.config.ConfigException$Missing: No configuration setting found for key 'some.flub'

Unfortunately, if the configuration is not present or cannot be parsed correctly
into the required type, an exception is thrown. You can ask for an option, or a 
Result of your type instead:

    val name = config[Option[String]]("some.name")  // Some(Alice)
    val flub1 = config[Option[String]]("some.flub") // None
    val age = config[Result[Int]]("some.age")       // \/-(32)
    val flub2 = config[Result[Int]]("some.flub")    // -\/(Err(com.typesafe.config.ConfigException$Missing: No configuration setting found for key 'some.flub'))

### Factory/Provider accessors

It is quite common that you want to configure some kind of Factory, Supplier
or Provider that implements some sort of interface like:

    trait Factory[A] { 
      def config: ConfigResult[A]
    }

where implementations are usually going to be provided as singleton objects:

    object FileFooFactory extends Factory[Foo] {
      def config: ConfigResult[Foo] = …
    }

There is a special `Module[A]` class that has a `Configuration.Accessor` that will
load an object instance if the config file holds the fully-qualified name of it:

    subconfig {
      foo = mypackage.FileFooFactory
    }

we can then use the following to get the instance:

    val fooModule = config[Module[Foo]]("subconfig.foo") // : Module[Foo]
    val foo = fooModule.get // Foo

## ConfigReader

Directly managing Configuration instances and passing them around as parameters
presents a challenge. It is usually far more convenient to use the `Reader` pattern
and create things that will construct an object of the required type when given
a `Configuration` object. `ConfigReader` is a `Reader` specialized to `Configuration`.

ConfigReaders are composable using for comprehensions.

However, mixing this with failure presents its more challenges and is not pleasant.

## ConfigResult

`ConfigResult` puts together `ResultT` and `ConfigReader` to provide an error
handling config reader that defers actual object construction until a Configuration
is supplied.

ConfigResults are composable using for comprehensions:

    val name: ConfigResult[String] = ConfigResult.get[String]("some.name")
    val age: ConfigResult[Int]     = ConfigResult.get[Int]("some.age")
    val flub: ConfigResult[Int]    = ConfigResult.get[Int]("some.flub")

    case class Person(name: String, age: Int)

    val person: ConfigResult[Person] =
      for {
        n <- name
        a <- age
      } yield Person(n, a)

and the actual value can be extracted given a config:

    import ConfigResult._ // needed for the syntax reader.extract*

    person.extract(config).run // scalaz.Id.Id[scalaz.\/[kadai.Invalid,Person]] = \/-(Person(Alice,32))

or slightly more conveniently:

    person.extractId(config) // scalaz.\/[kadai.Invalid,Person] = \/-(Person(Alice,32))

and fails if an error occurs, eg:

    val fails: ConfigResult[(Int, Person)] =
      for {
        n <- name
        a <- age
        f <- flub
      } yield f -> Person(n, a)

    fails.extractId(config) // scalaz.\/[kadai.Invalid,(Int, Person)] = -\/(Err(com.typesafe.config.ConfigException$Missing: No configuration setting found for key 'some.flub'))

You can of-course still request things that may fail directly:

    val notFail: ConfigResult[(Option[Int], Person)] =
      for {
        n <- name
        a <- age
        f <- ConfigResult.get[Option[Int]]("some.flub")
      } yield f -> Person(n, a)

     notfail.extractId(config) // \/-((None,Person(Alice,32)))
