/* 
 * Copyright 2012 Atlassian PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kadai.concurrent

import java.util.concurrent.atomic.{ AtomicBoolean, AtomicInteger, AtomicLong, AtomicReference }

/**
 * Extension to `AtomicReference` that allows more idiomatic usage.
 *
 * It adds a [[value]] that can be set with an update function:
 * {{{
 * value = (a: A) => calculate(a)
 * value = calculate(_)
 * }}}
 */
final class Atomic[@specialized(Int, Long, Boolean) A] private (access: Atomic.Access[A]) {
  /**
   * Update from the old to a new value and return the newly computed value.
   *
   * Note that the passed function must be pure as it may be re-computed if there is
   * contention when setting the updated value.
   */
  final def update(f: A => A): A =
    access.update(f)

  /** Update from the old to a new value and return a companion value computed at the same time. */
  final def updateAndGet[B](f: A => (A, B)): B =
    access.updateAndGet(f)

  /** Alias for get */
  def value =
    get

  /** Update method that allows the form: {{{value = updateFunction(_) }}} */
  def value_=(f: A => A): A =
    update(f)

  /** Get the current value as an Option */
  def option: Option[A] =
    Option(get)

  /** Atomically get the current value and set to null */
  def pop: Option[A] =
    Option(getAndSet(null.asInstanceOf[A]))

  def compareAndSet(from: A, to: A): Boolean =
    access.compareAndSet(from, to)

  def get: A =
    access.get

  def getAndSet(a: A): A =
    access.getAndSet(a)

  def lazySet(a: A): Unit =
    access.lazySet(a)

  def weakCompareAndSet(from: A, to: A): Boolean =
    access.weakCompareAndSet(from, to)
}

object Atomic {
  def apply[A: Accessor](a: A) =
    new Atomic(implicitly[Accessor[A]].apply(a))

  trait Accessor[@specialized(Int, Long, Boolean) A] {
    def apply: A => Access[A]
  }

  object Accessor {
    implicit def AnyRefAccessor[A <: AnyRef]: Accessor[A] =
      new Accessor[A] {
        def apply = new Access.Refs(_)
      }

    implicit val BooleanAccessor: Accessor[Boolean] =
      new Accessor[Boolean] {
        def apply = new Access.Booleans(_)
      }

    implicit val IntAccessor: Accessor[Int] =
      new Accessor[Int] {
        def apply = new Access.Ints(_)
      }

    implicit val LongAccessor: Accessor[Long] =
      new Accessor[Long] {
        def apply = new Access.Longs(_)
      }
  }

  /** Actual specialized implementation used as a delegate */
  //private[Atomic] 
  sealed trait Access[@specialized(Int, Long, Boolean) A] {
    def compareAndSet(from: A, to: A): Boolean
    def get(): A
    def getAndSet(a: A): A
    def lazySet(a: A): Unit
    def weakCompareAndSet(from: A, to: A): Boolean
    def update(f: A => A): A
    def updateAndGet[B](f: A => (A, B)): B
  }

  /** concrete implementations, need to be specialized for each val type, much c&p boilerplate, such wow. */
  //private[Atomic] 
  object Access {

    final class Refs[A <: AnyRef](a: A) extends AtomicReference[A](a) with Access[A] {
      final def update(f: A => A): A = {
        val old = get
        val a = f(old)
        if ((old eq get) && compareAndSet(old, a))
          a
        else
          update(f) // contention, retry
      }

      /** Update from the old to a new value and return a companion value computed at the same time. */
      @annotation.tailrec
      final def updateAndGet[B](f: A => (A, B)): B = {
        val old = get
        val (a, b) = f(old)
        if ((old eq get) && compareAndSet(old, a))
          b
        else
          updateAndGet(f)
      }
    }

    final class Ints(a: Int) extends AtomicInteger(a) with Access[Int] {
      final def update(f: Int => Int): Int = {
        val old = get
        val a = f(old)
        if ((old == get) && compareAndSet(old, a))
          a
        else
          update(f) // contention, retry
      }

      /** Update from the old to a new value and return a companion value computed at the same time. */
      @annotation.tailrec
      final def updateAndGet[B](f: Int => (Int, B)): B = {
        val old = get
        val (a, b) = f(old)
        if ((old == get) && compareAndSet(old, a))
          b
        else
          updateAndGet(f)
      }
    }

    final class Longs(a: Long) extends AtomicLong(a) with Access[Long] {
      final def update(f: Long => Long): Long = {
        val old = get
        val a = f(old)
        if ((old == get) && compareAndSet(old, a))
          a
        else
          update(f) // contention, retry
      }

      /** Update from the old to a new value and return a companion value computed at the same time. */
      @annotation.tailrec
      final def updateAndGet[B](f: Long => (Long, B)): B = {
        val old = get
        val (a, b) = f(old)
        if ((old == get) && compareAndSet(old, a))
          b
        else
          updateAndGet(f)
      }
    }

    final class Booleans(a: Boolean) extends AtomicBoolean(a) with Access[Boolean] {
      final def update(f: Boolean => Boolean): Boolean = {
        val old = get
        val a = f(old)
        if ((old == get) && compareAndSet(old, a))
          a
        else
          update(f) // contention, retry
      }

      /** Update from the old to a new value and return a companion value computed at the same time. */
      @annotation.tailrec
      final def updateAndGet[B](f: Boolean => (Boolean, B)): B = {
        val old = get
        val (a, b) = f(old)
        if ((old == get) && compareAndSet(old, a))
          b
        else
          updateAndGet(f)
      }
    }
  }
}
